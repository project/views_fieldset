<?php

namespace Drupal\Tests\views_fieldset\Kernel;

use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Tests\ViewTestData;
use Drupal\views\Views;

/**
 * Tests the field_set style plugin.
 *
 * @group views
 * @see \Drupal\views_fieldset\Plugin\views\style\FieldsetStyle
 */
class ViewsFieldsetStyleFieldsetTest extends ViewsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views_fieldset',
    'views_fieldset_test_views',
  ];

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_views_fieldset_style_fieldset'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);
    $this->installConfig(['views_fieldset', 'views_fieldset_test_views']);

    ViewTestData::createTestViews(get_class($this), ['views_fieldset_test_views']);
  }

  /**
   * Make sure that the field_set style markup is correct.
   */
  public function testStylePluginOptions() {
    $view = Views::getView('test_views_fieldset_style_fieldset');
    $output = $view->preview();
    $output = \Drupal::service('renderer')->renderRoot($output);

    // Check that fieldset element is present.
    $this->assertStringContainsString('<fieldset', $output, 'The fieldset element is present.');
    $this->assertStringContainsString('<legend>', $output, 'The fieldset element has a title.');
    $this->assertStringContainsString('class="description"', $output, 'The fieldset element has description.');
    $this->assertStringContainsString('class="field-content"', $output, 'The fieldset element has content.');


    // Unset tile and description in style options.
    $view->style_plugin->options['title'] = '';
    $view->style_plugin->options['description'] = '';

    $output = $view->preview();
    $output = \Drupal::service('renderer')->renderRoot($output);

    // Check that fieldset element is present.
    $this->assertStringContainsString('<fieldset', $output, 'The fieldset element is present.');
    $this->assertStringContainsString('<span class="fieldset-legend"></span>', $output, 'The fieldset element has no title.');
    $this->assertStringNotContainsString('class="description"', $output, 'The fieldset element has no description.');
    $this->assertStringContainsString('class="field-content"', $output, 'The fieldset element has content.');
  }

}
