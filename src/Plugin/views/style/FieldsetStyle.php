<?php

namespace Drupal\views_fieldset\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Fieldset style plugin to render rows as fieldset.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "fieldset",
 *   title = @Translation("Fieldset"),
 *   help = @Translation("Displays rows as fieldset."),
 *   theme = "views_view_fieldset",
 *   display_types = {"normal"}
 * )
 */
class FieldsetStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {

    $options = parent::defineOptions();
    $options['title'] = ['default' => ''];
    $options['description'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    parent::buildOptionsForm($form, $form_state);
    $options = ['' => $this->t('- None -')];
    $field_labels = $this->displayHandler->getFieldLabels(TRUE);
    $options += $field_labels;

    $form['title'] = [
      '#type' => 'select',
      '#title' => $this->t('Fieldset Title'),
      '#options' => $options,
      '#default_value' => $this->options['title'],
      '#description' => $this->t('Choose the title of fieldset.'),
      '#weight' => -48,
    ];

    $form['description'] = [
      '#type' => 'select',
      '#title' => $this->t('Fieldset Description'),
      '#options' => $options,
      '#default_value' => $this->options['description'],
      '#description' => $this->t('Optional fieldset description.'),
      '#weight' => -47,
    ];
  }

}
